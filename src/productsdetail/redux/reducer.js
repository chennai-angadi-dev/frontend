import { PRODUCT_DETAIL } from '../../helpers/type'

function ProductDetailListReducer(state = {}, action) {
  switch (action.type) {
    case PRODUCT_DETAIL.REQUEST:
      return Object.assign({}, state, {
        loadingPD: true
      });
    case PRODUCT_DETAIL.SUCCESSS:
      return Object.assign({}, state, {
        loadingPD: false,
        ProductDetailLists: action.ProductDetailList,
        statusPD: action.statusPD,
        messagePD: action.messagePD,
        namePD: action.namePD,
        count: action.count
      });
    case PRODUCT_DETAIL.ERROR:
      return Object.assign({}, state, {
        loadingPD: false,
        error: true,
        ProductDetailLists: [],
        statusPD: action.statusPD,
        messagePD: action.messagePD,
        namePD: action.namePD,
      });
    case PRODUCT_DETAIL.CLEAR:
      return Object.assign({}, state, {
        loadingPD: false,
        error: true,
        ProductDetailLists: [],
        statusPD: '',
        messagePD: '',
        namePD:''
      });
    default:
      return state;
  }
}

export default ProductDetailListReducer;
