import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { PRODUCT_DETAIL } from '../../helpers/type'
import {
  ProductDetailRequest,
  ProductDetailSuccess,
  ProductDetailError
} from './action';
import API from '../../redux/axiosConfig';
import { apiUrl } from '../../helpers/apiUrl';

export function* ProductDetailAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(ProductDetailRequest());
    const data = yield call(() => API.get(`${apiUrl.PRODUCT_DETAIL}/${action.payload}`, params));
    yield put(ProductDetailSuccess(data));

  } catch (error) {
    yield put(ProductDetailError());
  }
}

function* fetchProductDetailRootSaga() {
  yield all([
    yield takeEvery(PRODUCT_DETAIL.GET_LIST, ProductDetailAsync)
  ]);
}

const fetchProductDetailSaga = [
  fork(fetchProductDetailRootSaga),
];

export default fetchProductDetailSaga;
