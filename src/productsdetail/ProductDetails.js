import React, { useState, useEffect } from 'react'
import '../index.css';
import logo_img from '../assets/images/logo_img.png'
import productdetailbig_img from '../assets/images/productdetailbig_img.png'
import productdetailsm_img from '../assets/images/productdetailsm_img.png'
import like_img from '../assets/images/like_img.png'
import Products from '../landinpage/PopularProductDetail'
import underline_img from '../assets/images/underline_img.png'
import { FaSearchengin, FaSearch } from "react-icons/fa";
import Termsheaders from '../header/CommonHeader'
import termunderline_img from '../assets/images/termunderline_img.png'
import 'bootstrap/dist/css/bootstrap.min.css';
import { connect } from 'react-redux'
import { Carousel } from 'react-bootstrap';
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Container,
  Row,
  Col,
  Card,
  FontAwesomeIcon,
  InputGroup,
  InputGroupAddon,
  InputGroupText,
  Input,
  CardImg,
  ButtonGroup
} from 'reactstrap';
import { Link } from 'react-router-dom';
import Footer from '../components/Footer';
import { ProductDetailList } from '../productsdetail/redux/action'

function Productdetailpages(props) {

  const {
    ProductDetailLists,
    dispatch,
    statusPR,
    match
  } = props

  const matchId = match && match.params && match.params.id

  const [num, setNum] = useState(0);
  const [isOpen, setIsOpen] = useState(true);
  const [visible, setVisible] = useState(false);
  const [index, setIndex] = useState(0);

  useEffect(() => {
    if (matchId) {
      dispatch(ProductDetailList(matchId))
    } 
  }, [])

  const handleSelect = (selectedIndex, e) => {
    setIndex(selectedIndex);
  };

  const Increment = () => {
    setNum(num + 1)
  }

  const Decrement = () => {
    if (num > 0) {
      setNum(num - 1)
    }
    else {
      setNum(0)
      setVisible(true);
      const timer = setTimeout(() => {
        setVisible(false);
      }, 3000);
      return () => clearTimeout(timer);
    }
  }

  return (
    <Container className="productdetail-cotainer" fluid={true}>
      <div>
        <Termsheaders />
      </div>
      <div className="terms-heading">
        <h1 className="terms-head">ProductDetail</h1>
        <div className='terms-ul'>
          <img src={termunderline_img} />
        </div>
      </div>
      <Row className="mb-5">
        <Col xs={12} md={12} mx-auto>
          <Row>
            <Col xs={12} md={6} mx-auto>
              <div className="productdetail-card-bg">
                <Card className="product-detail-card">
                  <Col xs={12} md={7} className="mb-2">
                    <CardImg className="popular-product-card-img" src={productdetailbig_img} alt="Card image cap" />
                  </Col>
                  <Row>
                       <Carousel variant="dark" activeIndex={index} onSelect={handleSelect} className="prduct-slider" interval={3000}>
                    <Carousel.Item>
                      <img className="product-small-img w-70 h-70" src={productdetailsm_img} alt="Card image cap" />
                    </Carousel.Item>
                    <Carousel.Item>
                      <img className="product-small-img1" src={productdetailsm_img} alt="Card image cap" height="100%" width="100%" />
                    </Carousel.Item>
                    <Carousel.Item>
                      <img className="product-small-img2" src={productdetailsm_img} alt="Card image cap" height="100%" width="100%" />
                    </Carousel.Item>
                  </Carousel>
                  </Row>
                </Card>
              </div>
            </Col>
            <Col xs={12} md={5} mx-auto>
              <div className="product-card-bg1">
                <Col xs={12} md={2} >
                  <Button className="offbtn" size="sm">30% off</Button>
                </Col>
                <Col xs={12} md={8} >
                  <h4 className='product-detail-head'>
                  {
                   ProductDetailLists.name
                 }                    
                 </h4>
                </Col>
                <Col xs={12} md={12} >
                     <p className="productdetail-para">
                     {
                   ProductDetailLists.description
                 }  
                     </p>
                </Col>
                <Col xs={12} md={4} className="mb-1" >
                  <span class="product-details-rupee">&#x20B9;</span>
                  <span className='product-details-amount'>
                  {
                   ProductDetailLists.price
                 }                    
                 </span>
                </Col>
                <Col xs={12} md={4} className="mb-4" >
                  <h5 className="product-stock">In Stock</h5>
                </Col>
                <Row>
                  <Col xs={12} md={5} className="mb-4" >
                    <ButtonGroup className="product-details-plus-btn">
                    <Button className="product-details-minusbtn" onClick={Decrement}>-</Button>
                      <Button className="product-details-center" outline color="olivedrab">{num}</Button>
                      <Button className="product-details-minusbtn" onClick={Increment}>+</Button>
                    </ButtonGroup>
                  </Col>
                  <Col xs={12} md={7} className="product-details-kgbtn mb-4" >
                    <Button className="product-first-btn" size='sm'>0.5 kg</Button>{' '}
                    <Button className="product-first-btn1" outline color="olivedrab" size='sm' >1 kg</Button>{' '}
                    <Button className="product-first-btn1" outline color="olivedrab" size='sm' >2 kg</Button>{' '}
                  </Col>
                </Row>
                <Row className="producdetail-card">
                  <Col xs={12} md={8}>
                    <span><Button className='product-details-cart' color="success" size="md" >Add To Cart</Button>{' '}</span>
                  </Col>
                  <Col xs={12} md={4}>
                    <span><Button className='product-details-like' outline color="success" size="md"><img
                      src={like_img} alt='image' className="img-fluid"
                    /></Button>{' '}</span>
                  </Col>
                </Row>
              </div>
            </Col>
            <Col xs={12} md={12} mx-auto>
              <h1 className='popular-head'>Popular Product</h1>
            </Col>
            <Col xs={12} md={12} mx-auto className='mb-5'>
              <div className='popular-product-underline'>
                <img src={underline_img} alt='image' />
              </div>
            </Col>
            <Col xs={12} md={3} className="product1">
              <div className="product-size">
                <Products />
              </div>
            </Col>
            <Col xs={12} md={3} className="product2" >
              <div className="product-size">
                <Products />
              </div>
            </Col>
            <Col xs={12} md={3} className="product3" >
              <div className="product-size">
                <Products />
              </div>
            </Col>
          </Row>
        </Col>
      </Row>
      < Footer />
    </Container>
  );
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const mapStateToProps = state => {
  const { ProductDetailListReducer } = state
  return {
    ProductDetailLists: ProductDetailListReducer && ProductDetailListReducer.ProductDetailLists ? ProductDetailListReducer.ProductDetailLists : [],
    statusPR: ProductDetailListReducer && ProductDetailListReducer.statusPR ? ProductDetailListReducer.statusPR : '',
    id: ProductDetailListReducer && ProductDetailListReducer.id ? ProductDetailListReducer.id : '',
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Productdetailpages)