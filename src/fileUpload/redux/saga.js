import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { UPLOAD_IMAGE } from '../../helpers/type'
import {
  uploadRequest,
  uploadSuccess,
  uploadError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* uploadImageAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(uploadRequest());
    const data = yield call(() => API.post(apiUrl.FILEUPLOAD,  params ));
    yield put(uploadSuccess(data));
  } catch (error) {
    yield put(uploadError());
  }
}

function* uploadImageRootSaga() {
  yield all([
    yield takeEvery(UPLOAD_IMAGE.GET_LIST, uploadImageAsync)
  ]);
}

const uploadImageSaga = [
  fork(uploadImageRootSaga),
];

export default uploadImageSaga;
