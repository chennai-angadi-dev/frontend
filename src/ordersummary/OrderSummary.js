import React, { useState } from 'react'
import '../landinpage/index.css'
import { FaCheckCircle } from "react-icons/fa"
import Termsheaders from '../header/CommonHeader'
import termunderline_img from '../assets/images/termunderline_img.png'
import checkout_img from '../assets/images/checkout_img.png'
import totalline_img from '../assets/images/totalline_img.png'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardTitle,
  CardBody,
  CardText
} from 'reactstrap';

function SummaryPage() {

  return (
    <Container className="themed-container" fluid={true}>
      <div>
        <Termsheaders />
      </div>
      <div className="terms-heading">
        <h1 className="terms-head">Order Summary</h1>
        <div className='terms-ul'>
          <img src={termunderline_img} />
        </div>
      </div>
      <Col xs={12} md={12} mx-auto className="mb-5">
        <div>
          <Card className="checkout-card">
            <CardBody>
              <Col xs={12} md={12} mx-auto className="mb-5">
                <div className="summary-deliver ">
                  <h1 className="summary-click" ><FaCheckCircle /></h1>
                  <h5 className="summary-text">Your Order is successfully Delivered</h5>
                </div>
              </Col>
              <Row>
                <Col xs={12} md={12} mx-auto className="mb-3">
                  <h1 className="checkout-order">Order Summary</h1>
                </Col>
                <Col xs={12} md={2} mx-auto className="mb-5">
                  <CardImg className="checkout-img" src={checkout_img} alt="Card image cap" />
                </Col>
                <Col xs={12} md={4} mx-auto className="mb-5">
                  <div>
                    <p className="order-history-no mb-0">#20323652</p>
                    <p className="order-history-organic mb-0">Organic Strawberry</p>
                    <p className="order-history-kg mb-0">0.5kg</p>
                    <span class="categires-webrupee">&#x20B9;</span>
                    <span className='amount' >200</span>
                  </div>
                </Col>
                <Col xs={12} md={3} mx-auto className="mb-5">
                  <h1 className="order-history-date mb-0">Order Date</h1>
                  <p className="order-history-date1">02-04-2021</p>
                </Col>
                <Col xs={12} md={3} mx-auto className="mb-5">
                  <h1 className="order-history-deliver mb-0">Status</h1>
                  <p className="order-history-deliverd">Delivered</p>
                </Col>
              </Row>
              <Row className="mb-3">
                <Col xs={12} md={12} mx-auto className="mb-2">
                  <h1 className="checkout-order">Bill Details</h1>
                </Col>
                <Col xs={12} md={12} mx-auto>
                  <div className="checkout-bill">
                    <Row>
                      <Col xs={12} md={11} mx-auto>
                        <p className="checkout-total">Subtotal</p>
                      </Col>
                      <Col xs={12} md={1} mx-auto>
                        <div className="checkout-totals">
                          <span className="checkout-rupee">&#x20B9;</span>
                          <span className='checkout-rupee' >200</span>
                        </div>
                      </Col>
                      <Col xs={12} md={11} mx-auto>
                        <p className="checkout-total1">Coupon Applied</p>
                      </Col>
                      <Col xs={12} md={1} mx-auto className="mb-0">
                        <div className="checkout-totals1">
                          <span className="checkout-rupee">&#x20B9;</span>
                          <span className='checkout-rupee' >-50</span>
                        </div>
                      </Col>
                      <Col xs={12} md={12} mx-auto className="mb-0">
                        <img className="checkout-totalline" src={totalline_img} />
                      </Col>
                      <Col xs={12} md={11} mx-auto>
                        <p className="checkout-total2">Total</p>
                      </Col>
                      <Col xs={12} md={1} mx-auto>
                        <div className="checkout-totals2">
                          <span className="checkout-rupee1">&#x20B9;</span>
                          <span className='checkout-rupee1' >150</span>
                        </div>
                      </Col>
                    </Row>
                  </div>
                </Col>
              </Row>
              <Row className="mb-3">
                <CardTitle className="summary-second-head">
                  Delivery Address
                </CardTitle>
                <div className="sumamary-address mb-2">
                  <span className="checkout-second-head1 ">
                    William Jacob
                  </span>
                  <span className="checkout-contact">9876543210</span>
                  <p className="checkout-street">
                    Plot No.1069, Munusamy Salai,
                  </p>
                  <span className="checkout-street1">
                    K.K Nagar, Chennai – 600078.
                  </span>
                </div>
              </Row>
              <Col xs={12} md={12} mx-auto className="mb-0">
                <Button className="checkout-confirm-btn" color="sucess">
                  Continue Shopping
                </Button>
              </Col>
            </CardBody>
          </Card>
        </div>
      </Col>
    </Container>
  )
}
export default SummaryPage