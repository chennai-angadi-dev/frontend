import { VERIFY_OTP } from '../../helpers/type'


function fetchOTPListReducer(state = {}, action) {
  switch (action.type) {
    case VERIFY_OTP.REQUEST:
      return Object.assign({}, state, {
        loadingVO: true
      });
    case VERIFY_OTP.SUCCESSS:
      return Object.assign({}, state, {
        loadingVO: false,
        billionaireotpList: action.verifyOtp,
        statusVO: action.statusVO,
        messageVO: action.messageVO,
        count: action.count
      });
    case VERIFY_OTP.ERROR:
      return Object.assign({}, state, {
        loadingVO: false,
        error: true,
        billionaireotpList: [],
        statusVO: action.statusVO,
        messageVO: action.messageVO
      });
    case VERIFY_OTP.CLEAR:
      return Object.assign({}, state, {
        loadingVO: false,
        error: true,
        billionaireotpList: [],
        statusVO: '',
        messageVO: ''
      });
    default:
      return state;
  }
}

export default fetchOTPListReducer;
