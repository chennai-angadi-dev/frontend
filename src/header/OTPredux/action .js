import { VERIFY_OTP } from '../../helpers/type'
import { ERROR,STATUS_RESPONSE,SESSION } from '../../helpers/const'
import {setLocalStorage} from '../../helpers/utill'

export function verifyOtp(data) {
  return {
    type: VERIFY_OTP.VERIFY_DATA,
    payload: data
  }
}

export function verifyOtpRequest() {
  return {
    type: VERIFY_OTP.REQUEST
  }
}

export function verifyOtpSuccess(response) {
  const { data, status } = response
   if (response.status === STATUS_RESPONSE.SUCCESS_MSG) {
    setLocalStorage(SESSION.TOKEN, `Bearer ${response.data.token}`)
  return {
    type: VERIFY_OTP.SUCCESSS,
    statusVO: status,
    messageVO: response.message,
    verifyOtp: response
  }
  }
}

export function verifyOtpError(response) {
  return {
    type: VERIFY_OTP.ERROR,
    statusVO: ERROR.MSG,
    messageVO: response && response.message ? response.message : 'Something went wrong!'
  }
}

export function verifyOtpClear() {
  return { type: USER.CLEAR }
}

export function checkuserUniqueMobileNumber(data) {
  return {
    type: INTEREST_UNIQUE_MOBILE_NUMBER.INTEREST_UNIQUE_MOBILE_NUMBER,
    payload: data
  }
}
