import { USER } from '../../helpers/type'


function fetchUserListReducer(state = {}, action) {
  switch (action.type) {
    case USER.REQUEST:
      return Object.assign({}, state, {
        loadingFL: true
      });
    case USER.SUCCESSS:
      return Object.assign({}, state, {
        loadingFL: false,
        billionaireuserList: action.fetchList,
        statusFL: action.statusFL,
        messageFL: action.messageFL,
        count: action.count
      });
    case USER.ERROR:
      return Object.assign({}, state, {
        loadingFL: false,
        error: true,
        billionaireuserList: [],
        statusFL: action.statusFL,
        messageFL: action.messageFL
      });
    case USER.CLEAR:
      return Object.assign({}, state, {
        loadingFL: false,
        error: true,
        billionaireuserList: [],
        statusFL: '',
        messageFL: ''
      });
    default:
      return state;
  }
}

export default fetchUserListReducer;
