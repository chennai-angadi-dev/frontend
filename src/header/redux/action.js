import { USER,BILLIONAIRE,INTEREST_UNIQUE_MOBILE_NUMBER } from '../../helpers/type'
import { ERROR } from '../../helpers/const'


export function fetchList (params) {
  return {
    type: USER.GET_LIST,
    payload:params
  }
}

export function fetchListRequest () {
  return {
    type: USER.REQUEST
  }
}

export function fetchListSuccess (response) {

  const { data, status } = response
  return {
    type: USER.SUCCESSS,
    statusFL: status,
    messageFL: response.message,
    fetchList: response
  }
}

export function fetchListError (response) {
  return {
    type: USER.ERROR,
    statusFL: ERROR.MSG,
    messageFL: response && response.message ? response.message : 'Something went wrong!'
  }
}

export function fetchListClear () {
  return { type: USER.CLEAR }
}

export function checkuserUniqueMobileNumber (data) {
  return {
    type: INTEREST_UNIQUE_MOBILE_NUMBER.INTEREST_UNIQUE_MOBILE_NUMBER,
    payload: data
  }
}
