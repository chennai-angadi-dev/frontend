import { GET_ADD_LIST } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export function GetAddToCartList(data) {
  return {
    type: GET_ADD_LIST.GET_LIST,
    payload: data
  }
}

export function GetAddToCartRequest() {
  return {
    type: GET_ADD_LIST.REQUEST
  }
}

export function GetAddToCartSuccess(response) {
  const { data, status } = response
  return {
    type: GET_ADD_LIST.SUCCESSS,
    statusGetAdd: status,
    messageGetAdd: response.message,
    nameGetAdd: response.nameGetAdd,
    GetAddToCartList: data
  }
}

export function GetAddToCartError(response) {
  return {
    type: GET_ADD_LIST.ERROR,
    statusGetAdd: ERROR.MSG,
    messageGetAdd: response && response.message ? response.message : 'Something went wrong!'
  }
}