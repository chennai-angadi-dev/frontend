import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { GET_ADD_LIST } from '../../helpers/type'
import {
  GetAddToCartRequest,
  GetAddToCartSuccess,
  GetAddToCartError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* GetAddtocartListAsync(action) {
  const params = _.pickBy(action.payload, _.identity);

  try {
    yield put(GetAddToCartRequest());
    const data = yield call(() => API.get(apiUrl.OREDER,  params ));
    yield put(GetAddToCartSuccess(data));
  } catch (error) {
    yield put(GetAddToCartError());
  }
}

function* fetchGetAddToCartRootSaga() {
  yield all([
    yield takeEvery(GET_ADD_LIST.GET_LIST, GetAddtocartListAsync)
  ]);
}

const fetchGetAddToCartSaga = [
  fork(fetchGetAddToCartRootSaga),
];

export default fetchGetAddToCartSaga;
