import { LOGIN} from '../../helpers/type'
import { ERROR,STATUS_RESPONSE,SESSION } from '../../helpers/const'
import {setLocalStorage} from '../../helpers/utill'

export function loginList(data) {
  return {
    type: LOGIN.GET_LIST,
    payload: data
  }
}

export function loginListRequest() {
  return {
    type: LOGIN.REQUEST
  }
}

export function loginListSuccess(response) {
  const { data, status } = response
    return {
      type: LOGIN.SUCCESSS,
      statusLG: response.status,
      messageLG: response.message,
      loginList: response
    }
  }

export function loginListError(response) {
  return {
    type: LOGIN.ERROR,
    statusLG: ERROR.MSG,
    messageLG: response && response.message ? response.message : 'Something went wrong!'
  }
}

export function fetchListClear() {
  return { type: LOGIN.CLEAR }
}

