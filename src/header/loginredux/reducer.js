import { LOGIN } from '../../helpers/type'

function loginListReducer(state = {}, action) {
  switch (action.type) {
    case LOGIN.REQUEST:
      return Object.assign({}, state, {
        loadingLG: true
      });
    case LOGIN.SUCCESSS:
      return Object.assign({}, state, {
        loadingLG: false,
        billionaireloginList: action.loginList,
        statusLG: action.statusLG,
        messageLG: action.messageLG,
        count: action.count
      });
    case LOGIN.ERROR:
      return Object.assign({}, state, {
        loadingLG: false,
        error: true,
        billionaireloginList: [],
        statusLG: action.statusLG,
        messageLG: action.messageLG
      });
    case LOGIN.CLEAR:
      return Object.assign({}, state, {
        loadingLG: false,
        error: true,
        billionaireloginList: [],
        statusLG: '',
        messageLG: ''
      });
    default:
      return state;
  }
}

export default loginListReducer;
