export const SESSION = {
  TOKEN: 'token',
  EXPIRED: 'session_expired',
  EXPIRED_ERROR_CODE: 310,
  PLAYER_ID: 'playerId'
}

export const HEADER = {
  TOKEN: 'Authorization',
  CONTENT_TYPE: 'application/json',
  MULTIPART_CONTENT_TYPE: 'multipart/form-data',
  TIMEOUT: 120000,
  ALLOW_ACCESS: 'Access-Control-Allow-Origin',
}

export const ERROR = {
  INVALID_RESPONSE: 'Invalid response',
  MSG: 'error'
}

export const REGEX = {
  EMAIL: /^[a-zA-Z._-]+@[a-zA-Z.-]+\.[a-zA-Z]{2,4}$/,
  MOBILE: /^[0-9]{10}$/,
  LANDLINE_NUMBER: /^[0-9]{11}$/,
  DOMAIN: /^(?:[a-z0-9](?:[a-z0-9-]{0,61}[a-z0-9])?\.)+[a-z0-9][a-z0-9-]{0,61}[a-z0-9]$/,
  NUMBER: /^[0-9]*$/,
  WEBSITE_URL: /^(http:\/\/www\.|https:\/\/www\.|http:\/\/|https:\/\/)?[a-z0-9]+([\-\.]{1}[a-z0-9]+)*\.[a-z]{2,5}(:[0-9]{1,5})?(\/.*)?$/,
  OTP: /^[0-6]*$/,
  NAME: /^[a-zA-Z]+$/
}
export const USER = {
  TOKEN: 'token',
  ROLE: 'role'
}

export const JWT_EXPIRED = 'jwt expired';

export const LOCAL_STORAGE = {
  APP_CONFIG: 'app_config',
  ROLE: 'role',
  MOBILE_NUMBER: 'mobileNumber',
  EMAIL: 'email'
}

export const STATUS = {
  OK: 'ok',
  ERROR: 'error',
  ALL: 'ALL',
  INCOMPLETE: 'INCOMPLETE'
}

export const STATUS_RESPONSE = {
  SUCCESS_MSG: 'ok',
  ERROR: 'error',
  ALL: 'ALL',
  INCOMPLETE: 'INCOMPLETE'
}

export const WISHLIST_MESSAGE_RESPONSE = {
  ADD_SUCCESS_MSG: 'Product added to wishlist successfully',
  REMOVE_SUCCESS_MSG: 'Product removed from wishlist successfully',
  ERROR: 'error',
}

export const VERIFY_MESSAGE_RESPONSE = {
  SIGNUP_SUCCESS_MSG: 'User verified successfully',
  LOGIN_SUCCESS_MSG: 'Login successfully',
  ERROR: 'error',
  ALL: 'ALL',
  INCOMPLETE: 'INCOMPLETE'
}

export const EMPTY_DROPZONE = {
  MSG: 'upload profile',
  FILE_MSG: 'Upload file here. Only Json file are allowed.Allowed Size 1MB'
}

export const ERROR_MSG = {
  AVATAR_INVALID: 'Please upload valid image format!.',
  IMAGE_INVALID: 'Uploaded file is not a valid image. Only JPG, PNG and JPEG files are allowed.',
  JSON_INVALID: 'Uploaded file is not a valid json file.',
  PINCODE_INVALID: 'Invalid pincode.',
  PINCODE_REQUIRED: 'Pincode is required.',
}

export const FILE_FORMAT_TYPE = [
  'image/jpeg',
  'image/png',
  'image/jpg',
  'image/svg+xml',
  'image/JPEG',
  'image/PNG',
  'image/JPG'
]