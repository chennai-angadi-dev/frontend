import { DELETE_PROFILE } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export function DeleteProfileList(data) {
  return {
    type: DELETE_PROFILE.GET_LIST,
    payload: data
  }
}

export function DeleteProfileRequest() {
  return {
    type: DELETE_PROFILE.REQUEST
  }
}

export function DeleteProfileSuccess(response) {
  const { data, status } = response
  return {
    type: DELETE_PROFILE.SUCCESSS,
    statusDP: status,
    messageDP: response.message,
    DeleteProfileList: response
  }
}

export function DeleteProfileError(response) {
  return {
    type: DELETE_PROFILE.ERROR,
    statusDP: ERROR.MSG,
    messageDP: response && response.message ? response.message : 'Something went wrong!'
  }
}