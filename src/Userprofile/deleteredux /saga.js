import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { DELETE_PROFILE } from '../../helpers/type'
import {
  DeleteProfileRequest,
  DeleteProfileSuccess,
  DeleteProfileError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* DeleteProfileListAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(DeleteProfileRequest());
    const data = yield call(() => API.delete(`${apiUrl.PROFILE}/${action.payload}`, params));
    yield put(DeleteProfileSuccess(data));

  } catch (error) {
    yield put(DeleteProfileError());
  }
}

function* fetchDeleteProfileRootSaga() {
  yield all([
    yield takeEvery(DELETE_PROFILE.GET_LIST, DeleteProfileListAsync)
  ]);
}

const fetchDeleteProfileSaga = [
  fork(fetchDeleteProfileRootSaga),
];

export default fetchDeleteProfileSaga;
