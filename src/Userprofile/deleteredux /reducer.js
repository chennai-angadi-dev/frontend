import { DELETE_PROFILE } from '../../helpers/type'

function UserProfileReducer(state = {}, action) {
  switch (action.type) {
    case DELETE_PROFILE.REQUEST:
      return Object.assign({}, state, {
        loadingDP: true
      });
    case DELETE_PROFILE.SUCCESSS:
      return Object.assign({}, state, {
        loadingDP: false,
        DeleteProfileLists: action.DeleteProfileList,
        statusDP: action.statusDP,
        messageDP: action.messageDP,
        count: action.count
      });
    case DELETE_PROFILE.ERROR:
      return Object.assign({}, state, {
        loadingDP: false,
        error: true,
        ProfileLists: [],
        statusDP: action.statusDP,
        messageDP: action.messageDP
      });
    case DELETE_PROFILE.CLEAR:
      return Object.assign({}, state, {
        loadingDP: false,
        error: true,
        DeleteProfileLists: [],
        statusDP: '',
        messageDP: ''
      });
    default:
      return state;
  }
}

export default UserProfileReducer;
