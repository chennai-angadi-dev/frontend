import { USER_PROFILE } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export function UserProfileList(data) {
  return {
    type: USER_PROFILE.GET_LIST,
    payload: data
  }
}

export function UserProfileRequest() {
  return {
    type: USER_PROFILE.REQUEST
  }
}

export function UserProfileSuccess(response) {
  const { data, status } = response
  return {
    type: USER_PROFILE.SUCCESSS,
    statusAP: status,
    messageAP: response.message,
    UserProfileList: response
  }
}

export function UserProfileError(response) {
  return {
    type: USER_PROFILE.ERROR,
    statusAP: ERROR.MSG,
    messageAP: response && response.message ? response.message : 'Something went wrong!'
  }
}