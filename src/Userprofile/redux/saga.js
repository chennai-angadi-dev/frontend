import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { USER_PROFILE } from '../../helpers/type'
import {
  UserProfileRequest,
  UserProfileSuccess,
  UserProfileError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* UserProfileListAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(UserProfileRequest());
    const data = yield call(() => API.get(apiUrl.USERPROFILE,  params ));
    yield put(UserProfileSuccess(data));
  } catch (error) {
    yield put(UserProfileError());
  }
}

function* fetchUserProfileRootSaga() {
  yield all([
    yield takeEvery(USER_PROFILE.GET_LIST, UserProfileListAsync)
  ]);
}

const fetchUserProfileSaga = [
  fork(fetchUserProfileRootSaga),
];

export default fetchUserProfileSaga;
