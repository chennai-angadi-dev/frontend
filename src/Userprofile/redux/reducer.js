import { USER_PROFILE } from '../../helpers/type'

function UserProfileReducer(state = {}, action) {
  switch (action.type) {
    case USER_PROFILE.REQUEST:
      return Object.assign({}, state, {
        loadingUP: true
      });
    case USER_PROFILE.SUCCESSS:
      return Object.assign({}, state, {
        loadingUP: false,
        UserProfileLists: action.UserProfileList,
        statusUP: action.statusUP,
        messageUP: action.messageUP,
        count: action.count
      });
    case USER_PROFILE.ERROR:
      return Object.assign({}, state, {
        loadingUP: false,
        error: true,
        ProfileLists: [],
        statusUP: action.statusUP,
        messageUP: action.messageUP
      });
    case USER_PROFILE.CLEAR:
      return Object.assign({}, state, {
        loadingUP: false,
        error: true,
        UserProfileLists: [],
        statusUP: '',
        messageUP: ''
      });
    default:
      return state;
  }
}

export default UserProfileReducer;
