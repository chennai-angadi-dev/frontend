import { createStore, applyMiddleware } from 'redux'
import createSagaMiddleware from 'redux-saga'
import rootReducer from './rootReducer'
import rootSaga from './rootSaga'

const configureStore = () => {
  const preloadedState = {}
  const sagaMiddleware = createSagaMiddleware()
  const middlewares = applyMiddleware(sagaMiddleware)

  const store = createStore(rootReducer, preloadedState, middlewares);
  sagaMiddleware.run(rootSaga)
  return store
}

export default configureStore