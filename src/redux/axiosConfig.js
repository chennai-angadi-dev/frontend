import axios from 'axios'
import { getLocalStorage } from '../helpers/utill'
import { HEADER, ERROR, SESSION } from '../helpers/const'

const API = axios.create({
  headers: {
    [`${HEADER.TOKEN}`]: `${getLocalStorage(SESSION.TOKEN)}`,
    'Content-Type': HEADER.CONTENT_TYPE,
  },
  timeout: HEADER.TIMEOUT
})
API.interceptors.response.use(
  response => {
    if (typeof response.data !== 'object') {
      return Promise.error({ message: ERROR.INVALID_RESPONSE })
    }
    return response.data
  },
  error => {
    return Promise.reject(error)
  }
)

export default API