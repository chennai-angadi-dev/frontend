import React, { useState, useEffect } from 'react'
import '../landinpage/index.css';
import underline_img from '../assets/images/underline_img.png'
import CardMap from './CardLoopings'
import "../myprofile/myprofile.scss"
import { connect } from 'react-redux'
import { ProductsList } from './productredux/action'
import {
  Button,
  Container,
  Row,
  Col,
  ButtonGroup
} from 'reactstrap';
import { categorieList } from '../categoriepage/redux/action'
import { PictureAsPdfRounded } from '@material-ui/icons';
import _ from 'lodash'

function Categories(props) {

  const {
    dispatch,
    billionairecategorieList,
    ProductsLists
  } = props

  const [selectedId, setSelectedId] = useState();
  const [products, setProducts] = useState();

  const { data } = billionairecategorieList && billionairecategorieList

  useEffect(() => {
    dispatch(categorieList())
  }, [])

  useEffect(() => {
    if (ProductsLists) {
      if (ProductsLists) {
        setProducts(ProductsLists)
      }
    }
  }, [ProductsLists])

  useEffect(() => {
    dispatch(ProductsList())
  }, [])

  function categorieclick (id){
    setSelectedId(id)
    dispatch(ProductsList(id))
  }

  return (
    <Container>
      <Row>
        <Col xs={12} md={12} mx-auto>
          <div className="categories-head" >
          <h1 className='popular-head'>Categories</h1>
          </div>
        </Col>
      </Row>
      <Row>
        <Col xs={12} md={12} mx-auto className='catogries-underline mb-5'>
          <div>
            <img src={underline_img} alt='image' className="terms-ul" />
          </div>
        </Col>
      </Row>
      <Row className="mb-5">
        <Col xs={12} md={12} mx-auto>
          {
            data && data.map((item, i) => {
              return (
                <ButtonGroup >
                  <Button
                    color="green"
                    outline color="white"
                    className='categories-grp-btn'
                    className={item._id === 'Fruits & Vegetables' ? 'categories-fruitsbtn' : 'categories-grp-btn'}
                    onClick={() => categorieclick(item._id)}
                  >
                    {
                      item.name
                    }
                  </Button>
                </ButtonGroup>
              )
            })
          }
        </Col>
      </Row>
      <Row>
        <Col xs={12} md={3} >
          <CardMap />
        </Col>
      </Row>
    </Container>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const mapStateToProps = state => {
  const { fetcCategorigeListReducer, ProductsListReducer } = state
  return {
    billionairesubcategorieList: fetcCategorigeListReducer ? fetcCategorigeListReducer.billionairesubcategorieList : [],
    billionairecategorieList: fetcCategorigeListReducer && fetcCategorigeListReducer.billionairecategorieList ? fetcCategorigeListReducer.billionairecategorieList : [],
    ProductsLists: ProductsListReducer && ProductsListReducer.ProductsLists ? ProductsListReducer.ProductsLists : []
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Categories)