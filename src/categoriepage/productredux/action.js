import { PRODUCT_LIST } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export function ProductsList(data) {
  return {
    type: PRODUCT_LIST.GET_LIST,
    payload: data
  }
}

export function productsRequest() {
  return {
    type: PRODUCT_LIST.REQUEST
  }
}

export function productsSuccess(response) {
  const { data, status } = response
  return {
    type: PRODUCT_LIST.SUCCESSS,
    statusPR: status,
    messagePR: response.message,
    namePR: response.namePR,
    ProductsList: data
  }
}

export function productsError(response) {
  return {
    type: PRODUCT_LIST.ERROR,
    statusPR: ERROR.MSG,
    messagePR: response && response.message ? response.message : 'Something went wrong!'
  }
}