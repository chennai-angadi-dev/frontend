import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { CATEGORIES_LIST } from '../../helpers/type'
import {
  categorieRequest,
  categorieSuccess,
  categorieError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* categorieListAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(categorieRequest());
    const data = yield call(() => API.get(apiUrl.CATEGORY,  params ));
    yield put(categorieSuccess(data));
  } catch (error) {
    yield put(categorieError());
  }
}

function* fetchCategorieRootSaga() {
  yield all([
    yield takeEvery(CATEGORIES_LIST.GET_LIST, categorieListAsync)
  ]);
}

const fetchCategorieSaga = [
  fork(fetchCategorieRootSaga),
];

export default fetchCategorieSaga;
