import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { ADD_LIST } from '../../helpers/type'
import {
  AddToCartRequest,
  AddToCartSuccess,
  AddToCartError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* addtocartListAsync(action) {
  const params = action.payload;
  try {
    yield put(AddToCartRequest());
    const data = yield call(() => API.post(apiUrl.OREDER, action.payload));
    yield put(AddToCartSuccess(data));
  } catch (error) {
    yield put(AddToCartError());
  }
}

function* fetchAddToCartRootSaga() {
  yield all([
    yield takeEvery(ADD_LIST.GET_LIST, addtocartListAsync)
  ]);
}

const fetchAddToCartSaga = [
  fork(fetchAddToCartRootSaga),
];

export default fetchAddToCartSaga;
