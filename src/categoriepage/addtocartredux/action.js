import { ADD_LIST } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export function AddToCartList(data) {
  return {
    type: ADD_LIST.GET_LIST,
    payload: data
  }
}

export function AddToCartRequest() {
  return {
    type: ADD_LIST.REQUEST
  }
}

export function AddToCartSuccess(response) {
  const { data, status } = response
  return {
    type: ADD_LIST.SUCCESSS,
    statusADD: status,
    messageADD: response.message,
    nameADD: response.nameADD,
    AddToCartList: data,
    product_count : response.product_count
  }
}

export function AddToCartError(response) {
  return {
    type: ADD_LIST.ERROR,
    statusADD: ERROR.MSG,
    messageADD: response && response.message ? response.message : 'Something went wrong!'
  }
}