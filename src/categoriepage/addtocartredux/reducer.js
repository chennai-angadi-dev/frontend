import { ADD_LIST } from '../../helpers/type'

function AddToCartListReducer(state = {}, action) {
  switch (action.type) {
    case ADD_LIST.REQUEST:
      return Object.assign({}, state, {
        loadingADD: true,
        product_count: action.product_count
      });
    case ADD_LIST.SUCCESSS:
      return Object.assign({}, state, {
        loadingADD: false,
        AddToCartLists: action.AddToCartList,
        statusADD: action.statusADD,
        messageADD: action.messageADD,
        nameADD: action.nameADD,
        product_count: action.product_count
      });
    case ADD_LIST.ERROR:
      return Object.assign({}, state, {
        loadingADD: false,
        error: true,
        AddToCartLists: [],
        statusADD: action.statusADD,
        messageADD: action.messageADD,
        nameADD: action.nameADD,
      });
    case ADD_LIST.CLEAR:
      return Object.assign({}, state, {
        loadingADD: false,
        error: true,
        AddToCartLists: [],
        statusADD: '',
        messageADD: '',
        nameADD:''
      });
    default:
      return state;
  }
}

export default AddToCartListReducer;
