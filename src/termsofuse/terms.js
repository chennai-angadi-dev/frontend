import React, { useState } from 'react'
import '../landinpage/index.css'
import Termsheaders from '../header/CommonHeader'
import termunderline_img from './termunderline_img.png'
import Footer from '../components/Footer';


import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    NavLink,
    Button,
    Container,
    Row,
    Col,
    Card,
    CardImg,
    CardTitle,
    CardBody,
    CardText
} from 'reactstrap';

function TermsOfuse() {
    return (
        <Container className="themed-container" fluid={true}>
            <div>
                <Termsheaders />
            </div>
            <div className="terms-heading">
                <h1 className="terms-head">Terms Of Use</h1>
            <div className='terms-ul'>
                <img src={termunderline_img} />
            </div>
            </div>
            <Col xs={12} md={12} mx-auto className="mb-5">
                <div>
                    <Card className="terms-card">
                        <CardTitle className="terms-second-head">
                            Terms Of Use
                        </CardTitle>
                        <CardBody>
                            <CardText style={{ color: 'dimgray', margin: "10px" }}>Lorem Ipsum is simply dummy text
                            of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
                            dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                            it to make a type specimen book. It has survived not only five centuries, but also the leap into
                            electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                            release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                            software like Aldus PageMaker including versions of Lorem Ipsum.
                    </CardText>
                            <CardText style={{ color: 'dimgray', margin: "10px" }}>Lorem Ipsum is simply dummy text
                            of the printing and typesetting industry. Lorem Ipsum has been the industry's standard
                            dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled
                            it to make a type specimen book. It has survived not only five centuries, but also the leap into
                            electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the
                            release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing
                            software like Aldus PageMaker including versions of Lorem Ipsum.
                    </CardText>
                            <Button className="agree-btn">
                                Agree
                    </Button>
                        </CardBody>
                    </Card>
                </div>
            </Col>
            < Footer />
        </Container>

    )
}
export default TermsOfuse