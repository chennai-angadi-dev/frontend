import React, { useState, useEffect } from 'react'
import './index.css';
import underline_img from '../assets/images/underline_img.png'
import Products from './PopularProductDetail'
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink,
  Button,
  Container,
  Row,
  Col,
  Card,
  CardImg,
  CardTitle,
  CardBody,
  CardText
} from 'reactstrap';

function LikeProduct() {
  return (
    <Container className="mb-5">
      <Row>
        <Col xs={12} md={12} mx-auto className="popular-section mb-0" >
          <h1 className='popular-head'>Popular Product</h1>
        </Col>
        <Col xs={12} md={12} mx-auto className='Popular-head-ul mb-5'>
          <div>
            <img src={underline_img} alt='image' className="img-fluid " />
          </div>
        </Col>
        <Col xs={12} md={4} mx-auto>
          <div>
            <Products />
          </div>
        </Col>
        <Col xs={12} md={4} mx-auto>
          <div>
            <Products />
          </div>
        </Col>
        <Col xs={12} md={4} mx-auto>
          <div>
            <Products />
          </div>
        </Col>
      </Row>
    </Container>
  )
}
export default LikeProduct