import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { PROFILE } from '../../helpers/type'
import {
  ProfileRequest,
  ProfileSuccess,
  ProfileError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* ProfileListAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(ProfileRequest());
    const data = yield call(() => API.post(apiUrl.PROFILE,  params ));
    yield put(ProfileSuccess(data));
  } catch (error) {
    yield put(ProfileError());
  }
}

function* fetchProfileRootSaga() {
  yield all([
    yield takeEvery(PROFILE.GET_LIST, ProfileListAsync)
  ]);
}

const fetchProfileSaga = [
  fork(fetchProfileRootSaga),
];

export default fetchProfileSaga;
