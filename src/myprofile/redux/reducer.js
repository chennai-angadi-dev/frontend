import { PROFILE } from '../../helpers/type'

function fetcProfileReducer(state = {}, action) {
  switch (action.type) {
    case PROFILE.REQUEST:
      return Object.assign({}, state, {
        loadingCA: true
      });
    case PROFILE.SUCCESSS:
      return Object.assign({}, state, {
        loadingCA: false,
        ProfileLists: action.ProfileList,
        statusCA: action.statusCA,
        messageCA: action.messageCA,
        count: action.count
      });
    case PROFILE.ERROR:
      return Object.assign({}, state, {
        loadingCA: false,
        error: true,
        ProfileLists: [],
        statusCA: action.statusCA,
        messageCA: action.messageCA
      });
    case PROFILE.CLEAR:
      return Object.assign({}, state, {
        loadingCA: false,
        error: true,
        ProfileLists: [],
        statusCA: '',
        messageCA: ''
      });
    default:
      return state;
  }
}

export default fetcProfileReducer;
