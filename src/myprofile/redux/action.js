import { PROFILE } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export function ProfileList(data) {
  return {
    type: PROFILE.GET_LIST,
    payload: data
  }
}

export function ProfileRequest() {
  return {
    type: PROFILE.REQUEST
  }
}

export function ProfileSuccess(response) {
  const { data, status } = response
  return {
    type: PROFILE.SUCCESSS,
    statusCA: status,
    messageCA: response.message,
    ProfileList: response
  }
}

export function ProfileError(response) {
  return {
    type: PROFILE.ERROR,
    statusCA: ERROR.MSG,
    messageCA: response && response.message ? response.message : 'Something went wrong!'
  }
}