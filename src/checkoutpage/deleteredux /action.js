import { DELETE_ADDTOCART } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export  function DeleteAddtocart(data) {
  return {
    type: DELETE_ADDTOCART.GET_LIST,
    payload: data
  }
}

export function DeleteAddtocartRequest() {
  return {
    type: DELETE_ADDTOCART.REQUEST
  }
}

export function DeleteAddtocartSuccess(response) {
  const { data, status } = response
  return {
    type: DELETE_ADDTOCART.SUCCESSS,
    statusDAC: status,
    messageDAC: response.message,
    DeleteAddtocart: response
  }
}

export function DeleteAddtocartError(response) {
  return {
    type: DELETE_ADDTOCART.ERROR,
    statusDAC: ERROR.MSG,
    messageDAC: response && response.message ? response.message : 'Something went wrong!'
  }
}