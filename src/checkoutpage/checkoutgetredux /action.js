import { USER_GET_CHECKOUT } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export  function GetGetCheckout(data) {
  return {
    type: USER_GET_CHECKOUT.GET_LIST,
    payload: data
  }
}

export function  GetCheckoutRequest() {
  return {
    type: USER_GET_CHECKOUT.REQUEST
  }
}

export function  GetCheckoutSuccess(response) {
  const { data, status } = response
  return {
    type: USER_GET_CHECKOUT.SUCCESSS,
    statusCHT: status,
    messageCHT: response.message,
    GetCheckout: response
  }
}

export function  GetCheckoutError(response) {
  return {
    type: USER_GET_CHECKOUT.ERROR,
    statusCHT: ERROR.MSG,
    messageCHT: response && response.message ? response.message : 'Something went wrong!'
  }
}