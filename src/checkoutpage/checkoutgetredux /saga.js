import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { USER_CHECKOUT } from '../../helpers/type'
import {
  GetCheckoutRequest,
  GetCheckoutSuccess,
  GetCheckoutError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* GetCheckoutListAsync(action) {
  const params = action.payload;
  try {
    yield put(GetCheckoutRequest());
    const data = yield call(() => API.get(apiUrl.CHECKOUT, action.payload));
    yield put(GetCheckoutSuccess(data));
  } catch (error) {
    yield put(GetCheckoutError());
  }
}

function* fetchGetCheckoutRootSaga() {
  yield all([
    yield takeEvery(USER_CHECKOUT.GET_LIST, GetCheckoutListAsync)
  ]);
}

const fetchGetCheckoutSaga = [
  fork(fetchGetCheckoutRootSaga),
];

export default fetchGetCheckoutSaga;
