import React, { useState, useEffect } from 'react'
import '../landinpage/index.css';
import Mask_Group from '../assets/images/mask_Group.png'
import { RiDeleteBin5Fill } from "react-icons/ri"
import { WishList } from "./redux/action"
import { connect } from 'react-redux'
import {
  Button,
  Row,
  Col,
  Card,
  CardImg,
  CardTitle,
  CardBody
} from 'reactstrap';

function Wishlistcardmap(props) {

  const {
    MyWishLists,
    WishLists,
    dispatch,
    statusPR,
    match
  } = props
  const matchId = match && match.params && match.params.id

  useEffect(() => {
    if (matchId) {
      dispatch(WishList(matchId))
    } 
  }, [])
  return (
    <div>
      {
        MyWishLists && MyWishLists.map((item, i) => {
          return (
            <div key={i}>
              <Card>
                <Col xs={12} md={12} className="mb-2">
                  <CardImg className="Categories-card-img" src={Mask_Group} alt="Card image cap" />
                  <span className="deletebtn"><RiDeleteBin5Fill /></span>
                </Col>
                <CardBody className="whishlist-card">
                  <Row>
                    <Col xs={12} md={12} className="kilogram-btn mb-3">
                      <Button size='sm' className="categorie-kgbtn">0.5 kg</Button>{' '}
                      <Button outline color="olivedrab" size='sm' className="categorie-kgbtn1">1 kg</Button>{' '}
                      <Button outline color="olivedrab" size='sm' className="categorie-kgbtn1">2 kg</Button>{' '}
                    </Col>
                  </Row>
                  <Col xs={12} md={12} className="mb-2" >
                    <h5 className="wishlist-card-text mb-2">
                      Flower Mushroom
                      {
                        item.name
                      }
                    </h5>
                  </Col>
                  <Row>
                    <Col xs={12} md={12} className="categires-amount mb-2" >
                      <span class="categires-webrupee">&#x20B9;</span>
                      <span className='amount'>200</span>
                    </Col>
                  </Row>
                  <Row>
                    <Col xs={12} md={7}>
                      <span><Button className='wishlist-cart' color="oliverd" size="md" >Add To Cart</Button>{' '}</span>
                    </Col>
                  </Row>
                </CardBody>
              </Card>
            </div>
          )
        })
      }
    </div>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const mapStateToProps = state => {
  const { MyWishListReducer,WishListReducer } = state
  return {
    MyWishLists: MyWishListReducer && MyWishListReducer.MyWishLists ? MyWishListReducer.MyWishLists : [],
    WishLists: WishListReducer && WishListReducer.WishLists ? WishListReducer.WishLists : [],
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Wishlistcardmap)