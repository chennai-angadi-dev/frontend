import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { WISH_LIST } from '../../helpers/type'
import {
  WishListRequest,
  WishListSuccess,
  WishListError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* WishListAsync(action) {
  const params = action.payload;
  try {
    yield put(WishListRequest());
    const data = yield call(() => API.post(apiUrl.WISHLIST, action.payload));
    yield put(WishListSuccess(data));
  } catch (error) {
    yield put(WishListError());
  }
}

function* fetchWishlistRootSaga() {
  yield all([
    yield takeEvery(WISH_LIST.GET_LIST, WishListAsync)
  ]);
}

const fetchWishlistSaga = [
  fork(fetchWishlistRootSaga),
];

export default fetchWishlistSaga;
