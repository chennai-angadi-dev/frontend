import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { MY_WISH_LIST } from '../../helpers/type'
import {
  MyWishListRequest,
  MyWishListSuccess,
  MyWishListError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* MyWishListAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(MyWishListRequest());
    const data = yield call(() => API.get(apiUrl.WISHLIST,params ));
    yield put(MyWishListSuccess(data));
  } catch (error) {
    yield put(MyWishListError());
  }
}

function* fetchMyWishListRootSaga() {
  yield all([
    yield takeEvery(MY_WISH_LIST.GET_LIST, MyWishListAsync)
  ]);
}

const fetchMyWishListSaga = [
  fork(fetchMyWishListRootSaga),
];

export default fetchMyWishListSaga;
