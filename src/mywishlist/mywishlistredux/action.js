import { MY_WISH_LIST } from '../../helpers/type'
import { ERROR } from '../../helpers/const'

export  function MyWishList(data) {
  return {
    type: MY_WISH_LIST.GET_LIST,
    payload: data
  }
}

export function MyWishListRequest() {
  return {
    type: MY_WISH_LIST.REQUEST
  }
}

export function MyWishListSuccess(response) {
  const { data, status } = response
  return {
    type: MY_WISH_LIST.SUCCESSS,
    statusMWL: status,
    loadingMWL: response.message,
    MyWishList: data
  }
}

export function MyWishListError(response) {
  return {
    type: MY_WISH_LIST.ERROR,
    statusMWL: ERROR.MSG,
    loadingMWL: response && response.message ? response.message : 'Something went wrong!'
  }
}