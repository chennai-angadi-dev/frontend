import React from 'react';
import { Pagination, PaginationItem, PaginationLink } from 'reactstrap';
import Homepage from '../Home page/index' 

const Examplepage = (props) => {
  return (
    <Pagination size="sm" aria-label="Page navigation example">
      <PaginationItem>
        <PaginationLink first href="Homepage" />
      </PaginationItem>
      <PaginationItem>
        <PaginationLink previous href="#" />
      </PaginationItem>
      <PaginationItem>
        <PaginationLink href="#">
          1
        </PaginationLink>
      </PaginationItem>
      <PaginationItem>
        <PaginationLink href="#">
          2
        </PaginationLink>
      </PaginationItem>
      <PaginationItem>
        <PaginationLink href="#">
          3
        </PaginationLink>
      </PaginationItem>
      <PaginationItem>
        <PaginationLink next href="#" />
      </PaginationItem>
      <PaginationItem>
        <PaginationLink last href="#" />
      </PaginationItem>
    </Pagination>
  );
}

export default Examplepage;