import {
  all, call, put, takeEvery, fork
} from 'redux-saga/effects';
import _ from 'lodash';
import { ADDRESS_PROFILE } from '../../helpers/type'
import {
  AddressProfileRequest,
  AddressProfileSuccess,
  AddressProfileError
} from './action';
import API from '../../redux/axiosConfig';
import {apiUrl} from '../../helpers/apiUrl';

export function* AddressProfileListAsync(action) {
  const params = _.pickBy(action.payload, _.identity);
  try {
    yield put(AddressProfileRequest());
    const data = yield call(() => API.put(apiUrl.USERPROFILE,  params ));
    yield put(AddressProfileSuccess(data));
  } catch (error) {
    yield put(AddressProfileError());
  }
}

function* fetchAddressProfileRootSaga() {
  yield all([
    yield takeEvery(ADDRESS_PROFILE.GET_LIST, AddressProfileListAsync)
  ]);
}

const fetchAddressProfileSaga = [
  fork(fetchAddressProfileRootSaga),
];

export default fetchAddressProfileSaga;
