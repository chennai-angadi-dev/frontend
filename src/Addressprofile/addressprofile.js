import React, { useState, useEffect } from 'react';
import {
  Card, CardImg, CardBody, Button, Input, Alert
} from 'reactstrap';
import { Row, Col } from 'reactstrap';
import AddLocationIcon from '@material-ui/icons/AddLocation';
import { green } from '@material-ui/core/colors';
import "./addressprofile.scss";
import Vector from './images/Vector.png';
import Footer from '../components/Footer';
import Termsheaders from '../header/CommonHeader';
import { makeStyles, useTheme } from '@material-ui/core/styles';
import termunderline_img from '../assets/images/termunderline_img.png'
import { ADRESS_FIELD_MGS } from '../helpers/message'
import { connect } from 'react-redux'
import {
  LOCAL_STORAGE,
  REGEX,
  STATUS_RESPONSE,
  VERIFY_MESSAGE_RESPONSE,
  JWT_EXPIRED,
} from '../helpers/const'
import { Link } from 'react-router-dom';
import { AddressProfileList } from './redux/action'
import { UserProfileList } from '../Userprofile/redux/action'
import { REGISTERATION_FORM_MSGS, LOGIN_MGS } from '../helpers/message'

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },

  largeIcon: {
    marginBottom: 5,
    fontSize: '18px',
    color: "#83A43E",
  }
}));
const Example = (props) => {

  const {
    dispatch,
    match,
    statusAP
  } = props

  const matchId = match && match.params && match.params.id
  const [errors, setErrors] = useState({})
  const [flat_no, setFlat_no] = useState()
  const [toggle, setToggle] = useState(false)
  const [editMode, setEditMode] = useState(false)
  const [toggleerror, setToggleerror] = useState(false)
  const [formData, setFormData] = useState({
    first_name: '',
    last_name: '',
    mobile_number: '',
    flat_no: '',
    state: '',
    building_name: '',
    street: '',
    area: '',
    city: '',
    pincode: '',
    landmark: '',
  })
  const classes = useStyles();

  const Reset = (formData) => {
    Reset()
  }

  useEffect(() => {
    if (matchId) {
      dispatch(UserProfileList(matchId))
      setEditMode(true)
    } else {
      setEditMode(false)
    }
  }, [])

  const validate = () => {
    let valid = true
    const errors = {}
    if (!formData.mobile_number) {
      errors.mobile_number = REGISTERATION_FORM_MSGS.USER_MOBILENUMBER_REQUIRED
      valid = false
    } else if (!REGEX.MOBILE.test(formData.mobile_number)) {
      errors.mobile_number = REGISTERATION_FORM_MSGS.MOBILE_NUMBER_INVALID
      valid = false
    }
    if (!formData.first_name) {
      errors.first_name = REGISTERATION_FORM_MSGS.FIRST_NAME_REQUIRED
      valid = false
    } else if (!REGEX.NAME.test(formData.first_name)) {
      errors.first_name = REGISTERATION_FORM_MSGS.FIRST_NAME_INVALID
      valid = false
    }
    if (!formData.last_name) {
      errors.last_name = REGISTERATION_FORM_MSGS.LAST_NAME_REQUIRED
      valid = false
    } else if (!REGEX.NAME.test(formData.last_name)) {
      errors.last_name = REGISTERATION_FORM_MSGS.LAST_NAME_INVALID
      valid = false
    }
    if (!formData.flat_no) {
      errors.flat_no = ADRESS_FIELD_MGS.ADRESS_REQUIRED
      valid = false
    }
    if (!formData.state) {
      errors.state = ADRESS_FIELD_MGS.ADRESS_REQUIRED
      valid = false
    }
    if (!formData.building_name) {
      errors.building_name = ADRESS_FIELD_MGS.ADRESS_REQUIRED
      valid = false
    }
    if (!formData.street) {
      errors.street = ADRESS_FIELD_MGS.ADRESS_REQUIRED
      valid = false
    }
    if (!formData.area) {
      errors.area = ADRESS_FIELD_MGS.ADRESS_REQUIRED
      valid = false
    }
    if (!formData.city) {
      errors.city = ADRESS_FIELD_MGS.ADRESS_REQUIRED
      valid = false
    }
    if (!formData.pincode) {
      errors.pincode = ADRESS_FIELD_MGS.ADRESS_REQUIRED
      valid = false
    }
    if (!formData.landmark) {
      errors.landmark = ADRESS_FIELD_MGS.ADRESS_REQUIRED
      valid = false
    }
    setErrors(errors)
    return valid
  }

  function loginChange(e) {
    const { name, value } = e.target;
    setFormData(formData => ({ ...formData, [name]: value }));
  }
  function loginSubmit() {
    if (validate()) {
      dispatch(AddressProfileList(formData))
    }
  }

  useEffect(() => {
    if (statusAP === STATUS_RESPONSE.SUCCESS_MSG) {
      setToggle(true)
      const timer = setTimeout(() => {
        setToggle(false);
      }, 3000);
      props.history.push('/userprofile/$')
    } else if (statusAP === STATUS_RESPONSE.ERROR) {
      setToggleerror(true)
      const timer = setTimeout(() => {
        setToggleerror(false);
      }, 3000);
    }
  }, [statusAP])

  return (
    <div className="container-fluid prof">
      < Termsheaders />
      <div className="terms-heading">
        <h1 className="terms-head">Address Profile</h1>
        <div className='terms-ul'>
          <img src={termunderline_img} />
        </div>
      </div>
      <div className="img-circ">
        <CardImg className="img-face" src={Vector} />
      </div>
      <div className="card-title">
        <Card>
          <i class="fa fa-pencil-square-o" aria-hidden="true"></i>
          <CardBody className="card-name">
            <h6 >William Jacob</h6>
            <h6 className="case-add">9876543210</h6>
            <h6 className="case-add">shenll@gmail.com</h6>
          </CardBody>
        </Card>
      </div>
      <div className="userdetails">
        <Card>
          <Alert color="danger" isOpen={toggleerror} fade={false}>
            Address Error
          </Alert>
          <Alert color="success" isOpen={toggle} fade={false}>
            Address added successfully
          </Alert>
          <CardBody className="card-add">
            <p className="manage">Manage Address </p>
            <div className="add-details">
              <div className="add-new">
                <Link to={`/addressprofile/$`}>
                  <AddLocationIcon className={classes.largeIcon} />
                  <span className="add-address">  Add New Address </span>
                </Link>
              </div>
              <Row>
                <Col xs="5">
                  <Input
                    className="textfield-bg"
                    type="text" size="sm"
                    placeholder="First name"
                    name="first_name"
                    value={formData.first_name}
                    onChange={loginChange}
                  />
                  {
                    errors.first_name && (<div className='text-danger'> {errors.first_name} </div>)
                  }
                </Col>
                <Col xs="5">
                  <Input
                    className="textfield"
                    type="text" size="sm"
                    placeholder="Last name"
                    name="last_name"
                    value={formData.last_name}
                    onChange={loginChange}
                  />
                  {
                    errors.last_name && (<div className='text-danger'> {errors.last_name} </div>)
                  }
                </Col>
                <Col xs="5">
                  <Input
                    className="textfield-bg"
                    type="text" size="sm"
                    placeholder="Mobile Number"
                    name="mobile_number"
                    value={formData.mobile_number}
                    onChange={loginChange}
                    maxLength="10"
                  />
                  {
                    errors.mobile_number && (<div className='text-danger'> {errors.mobile_number} </div>)
                  }
                </Col>
                <Col xs="5">
                  <Input
                    className="textfield"
                    type="text" size="sm"
                    placeholder="flat_no"
                    name="flat_no"
                    flat_no={flat_no}
                    value={formData.flat_no || ''}
                    onChange={loginChange}
                    maxLength="4"
                  />
                  {
                    errors.flat_no && (<div className='text-danger'> {errors.flat_no} </div>)
                  }
                </Col>
                <Col xs="7">
                  <Input className="textfield-bg" type="text" size="sm" placeholder="Building name"
                    name="building_name"
                    value={formData.building_name || ''}
                    onChange={loginChange}
                    maxLength="10"
                  />
                  {
                    errors.building_name && (<div className='text-danger'> {errors.building_name} </div>)
                  }
                </Col>
                <Col xs="5">
                  <Input className="textfield-area" type="text" size="sm" placeholder="Area"
                    name="area"
                    value={formData.area || ''}
                    onChange={loginChange}
                    maxLength="20"
                  />
                  {
                    errors.area && (<div className='text-danger'> {errors.area} </div>)
                  }
                </Col>
                <Col xs="7">
                  <Input className="textfield-bg" type="text" size="sm" placeholder="Street"
                    name="street"
                    value={formData.street || ''}
                    onChange={loginChange}
                    maxLength="10"
                  />
                  {
                    errors.street && (<div className='text-danger'> {errors.street} </div>)
                  }
                </Col>
                <Col xs="5">
                  <Input className="textfield-area" type="text" size="sm" placeholder="City"
                    name="city"
                    value={formData.city || ''}
                    onChange={loginChange}
                    maxLength="10"
                  />
                  {
                    errors.city && (<div className='text-danger'> {errors.city} </div>)
                  }
                </Col>
                <Col xs="7">
                  <Input className="textfield-bg" type="text" size="sm" placeholder="Pincode"
                    name="pincode"
                    value={formData.pincode || ''}
                    onChange={loginChange}
                    maxLength="6"
                  />
                  {
                    errors.pincode && (<div className='text-danger'> {errors.pincode} </div>)
                  }
                </Col>
                <Col xs="5">
                  <Input className="textfield-area" type="text" size="sm" placeholder="State"
                    name="state"
                    value={formData.state}
                    onChange={loginChange || ''}
                    maxLength="10"
                  />
                  {
                    errors.state && (<div className='text-danger'> {errors.state} </div>)
                  }
                </Col>
                <Col xs="7">
                  <Input className="textfield-bg" type="text" size="sm" placeholder="Landmark"
                    name="landmark"
                    value={formData.landmark}
                    onChange={loginChange || ''}
                    maxLength="10"
                  />
                  {
                    errors.landmark && (<div className='text-danger'> {errors.landmark} </div>)
                  }
                </Col>
                <Col xs="5">
                </Col>
                <Col xs="7">
                  <Button className="button-bg" color="success"
                    onClick={loginSubmit}
                  >Save</Button>{' '}
                  <Link to={`/addressprofile/`}>
                    <Button className="cancel-add"
                    onClick={Reset}
                    >cancel</Button>{' '}
                  </Link>
                </Col>
              </Row>
            </div>
          </CardBody>
        </Card>
      </div>
      <Footer />
    </div>
  );
};


const mapDispatchToProps = dispatch => {
  return {
    dispatch
  }
}

const mapStateToProps = state => {
  const { AddressProfileReducer } = state
  return {
    statusAP: AddressProfileReducer && AddressProfileReducer.statusAP ? AddressProfileReducer.statusAP : [],
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(Example)