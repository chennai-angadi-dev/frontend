import React from "react";
import "./Footer.scss";
import img1 from "./img1.png";
const Footer = () => {
  return (
    <div className="main-footer">
      <div className="container">
        <div className="row">
          <div className="col-md-4 col-sm-8 ">
            <img src={img1} alt={"logo"} width="130" height="90" />
            <p className="footer-head">Lorem Ipsum is simply dummy text of the printing and typesetting industry.</p>
          </div>
          <div className="col-md-2 col-sm-5">
            <h4 className="link">Link</h4>
            <ul className="list-unstyled">
              <div className="list">
                <li className="home">Home </li>
                <li className="home">My wishlist</li>
                <li className="home">Order History </li>
              </div>
            </ul>
          </div>
          <div className="col-md-2 col-sm-5">
            <h5 className="support">Support</h5>
            <ul className="list-unstyled support">
              <div className="list">
                <li className="home">Settings </li>
                <li className="home">Help</li>
              </div>
            </ul>
          </div>
          <div className="col-md-4 col-sm-8 ">
            <h5 className="contact-us">Contact Us</h5>
            <ul className="list-unstyled contact-us ">
              <div className="list">
                <li className="foo">
                  <i class="fa fa-envelope-o" aria-hidden="true"></i>
                  <span className="home">info@Shenll.com</span>
                </li>
                <li className="foo">
                  <i class="fa fa-life-ring" aria-hidden="true"></i>
                  <span className="home">www.shenll.com</span>
                </li>
                <li className="foo">
                  <i class="fa fa-phone-square" aria-hidden="true"></i>
                  <span className="home">+91-9791192509 </span>
                </li>
              </div>
            </ul>
          </div>
          <div className="footer-bottom">
            <p> Copyright <i class="fa fa-copyright" aria-hidden="true"></i> 2020 Freshio .All Rights Reserved.</p>
          </div>
        </div>
      </div>
    </div>
  );
}
export default Footer;